<h1>IrrigaTech - Plantações Inteligentes</h1>
<a href='http://sistemadeirrigacao.xyz/index.php'>IrrigaTech - Plantações Inteligentes (Versão Beta)</a>
<h2>Introdução</h2>
  <p>O projeto proposto, terá como objetivo contemplar as técnicas e metodologias de desenvolvimento aprendidas no ambiente acadêmico, 
  juntamente com estes preceitos serão implementadas técnicas de comunicação de hardware e software baseados nos conceitos de Internet das Coisas (IoT).
  Para atender as expectativas estabelecidas foi proposto o desenvolvimento de um Sistema de Irrigação Doméstico capaz de ser controlado remotamente.</p>
   <p>Este projeto será utilizado como objeto de estudo da disciplina de Projeto de Desenvolvimento do 5º semestre da Faculdade de Tecnologia Senac Pelotas (RS), o projeto conta com a orientação do professor Dr. Edécio Iepsen.</p>

<p>Situação Atual do Projeto:<strong> Em Desenvolvimento</strong> :construction:</p>

<h2>Pré Requisitos</h2>
<h3>Arduino Uno R3</h3>

<p>A placa [Arduino Uno R3](https://store.arduino.cc/usa/arduino-uno-rev3) é uma placa baseada no microcontolador  ATmega328. Constituido de 14 pinos
de entrada/saída digital, e 6 entradas analógica, um cristal de 16MHz, acompanhado de uma entrada 
USB. Para utilizar a placa Arduino Uno será nescessário fazer o download da IDE de desenvolvimento
no próprio site do hardware [Arduino](https://www.arduino.cc/en/Main/Software), ou também é possivel utilizar a IDE online disponibilizada no site.</p>

<p>[Site Oficial do Arduino](https://www.arduino.cc/)</p>


<h3>Módulo WiFi ESP8266 NodeMcu ESP-12</h3>
  <p>O [Módulo Wifi ESP8266 NodeMcu](https://www.filipeflop.com/produto/modulo-wifi-esp8266-nodemcu-esp-12/) é uma placa  desenvolvida na china
que combina o chip ESP826, uma interface usb-serial e um regulador de tenção de 3.3v.
A programação pode ser feita usando a linguagem LUA ou através da IDE do Arduino.</p>

<h2>Clonar</h2>
<p>Para efetuar a clonagem do projeto, utilizando os <i>links</i> disponiveis no repositório GitLab
é nescessário possuir o <i>Software</i> Git instalado na sua máquina. Para mais informações de instalação
acesse o site do [Git](https://git-scm.com/) .Caso você ja possua o Git em sua máquina você pode
baixar este projeto através dos <i>Links</i> disponíveis abaixo.</p>

<h2>Clone com SSH</h2>
 <ul>
 <li>git clone git@gitlab.com:senac-tcc-2019/rafael-garcia.git</li>
 </ul>
 
 <h2>Clone com HTTPS</h2>
 <ul>
 <li>git clone https://gitlab.com/senac-tcc-2019/rafael-garcia.git</li>
 </ul>
 
 <h2>Liscença</h2>
 <p>Este projeto conta com a seguinte licença de uso: Instituto de Tecnologia de <i>Massachusetts</i> [MIT](https://pt.wikipedia.org/wiki/Licen%C3%A7a_MIT).</p>
