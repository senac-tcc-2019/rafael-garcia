(function($){
        //Extendento funcao jquery
        $.fn.customPaginate = function(options)
        {
            var paginationContainer = this;
            var itensToPaginate;
         

            var defaults ={
              itensPerPage : 1
            };

            var settings={};

            $.extend(settings,defaults,options);

            var itensPerPage = settings.itensPerPage;

            itensToPaginate = $(settings.itensToPaginate);

            var numberOfPaginationLinks = Math.ceil((itensToPaginate.length / itensPerPage));

            $("<ul></ul>").prependTo(paginationContainer);

            for(var index = 0; index < numberOfPaginationLinks; index++)
            {
                paginationContainer.find("ul").append("<li>" + (index + 1 ) +"</li>");
            }

            itensToPaginate.filter(":gt("+ (itensPerPage -1) +")").hide();

            paginationContainer.find("ul li").on('click',function(){
                var linkNumber = $(this).text();
                var itensToHide = itensToPaginate.filter(":lt("+ ((linkNumber -1) * itensPerPage) +")");
                $.merge(itensToHide,itensToPaginate.filter(":gt("+ ((linkNumber * itensPerPage) -1) +")"));
                itensToHide.hide();

                var itensToShow = itensToPaginate.not(itensToHide);
                itensToShow.show();


            });
        }

}(jQuery));