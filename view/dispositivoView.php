<?php
error_reporting(0);
session_start();
include_once '../dao/conexao.php';
require_once '../controller/dispositivoController.php';

date_default_timezone_set("America/Sao_Paulo");
setlocale(LC_ALL, 'pt_BR');

if ((!isset($_SESSION['username']) == true) && (!isset($_SESSION['senha']) == true)) {
    unset($_SESSION['username']);
    unset($_SESSION['senha']);

    header("location: login.php");
} else {
    $sql = "SELECT imagem FROM tb_usuario WHERE idtb_usuario= '" . $_SESSION['idtb_usuario'] . "'";
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    $row = mysqli_fetch_array($result);
    $imagem = $row['imagem'];
    if (empty($imagem)) {
        $imagem_caminho = "../dist/imagens/no-image-icon.png";
    } else {
        $imagem_caminho = "../dist/imagens/" . $imagem;
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE = edge">
    <title>IrrigaTech</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width = device-width, initial-scale = 1, maximum-scale = 1, user-scalable = no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!--estilo paginação-->
    <link rel="stylesheet" href="../bower_components/css/paginacao.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>


<style>
li {
    list-style: none;
}
</style>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="home.php" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>I</b>T</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Irriga</b>Tech</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="dist/img/user4-128x128.jpg" class="img-circle"
                                                        alt="User Image">
                                                </div>
                                                <h4>
                                                    Reviewers
                                                    <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <!-- Tasks: style can be found in dropdown.less -->
                        <li class="dropdown tasks-menu">
                            <ul class="dropdown-menu">

                                <li>
                                    <ul class="menu">
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo $imagem_caminho; ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo$_SESSION['username']; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php echo $imagem_caminho; ?>" class="img-circle" alt="User Image">

                                    <p>
                                        <?php echo$_SESSION['username']; ?>

                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="http://localhost/irrigatech_git/rafael-garcia/controller/logOutController.php"
                                            class="btn btn-default btn-flat">Sair</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo $imagem_caminho; ?>" style="width:80px;height:50px; border-radius: 50%;"
                            alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?php echo ucfirst($_SESSION['username']); ?> </p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                    class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MENU PRINCIPAL</li>

                    <li><a href="homeView.php"><i class="fa fa-feed"></i> <span>Conexões</span></a></li>
                    <li><a href="dispositivoView.php"><i class="fa fa-plus-circle"></i> <span>Novo
                                Dispositivo</span></a></li>
                    <li><a href="areaView.php"><i class="fa fa-plus-circle"></i> <span>Area de Monitoramento</span></a>
                    </li>
                    <li><a href="recursosView.php"><i class="fa fa-tint"></i> <span>Recursos</span></a></li>
                    <li><a href="agendamentoView.php?action=2"><i class="fa fa-calendar-o"></i> <span>Agendar</span></a>
                    </li>
                    <li><a href="relatorioView.php"><i class="fa fa-file-text"></i> <span>Relatório</span></a></li>
                    <li><a href="#"><i class="fa  fa-gears"></i> <span>Configurações</span></a></li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->

            <section class="content-header">
                <h1>
                    Home
                    <small>Novo Dispositivo</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="homeView.php"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li class="active">Novo Dispositivo</li>
                </ol>

            </section>

            <!-- Main content -->
            <section class="content">
                <?php if (isset($_SESSION['mensagem'])): ?>
                <div class="warning">
                    <div class="alert alert-<?= $_SESSION['tipo_mensagem'] ?>">
                        <?php
                                echo$_SESSION['mensagem'];
                                unset($_SESSION['mensagem']);
                      ?>
                    </div>
                </div>
                <?php endif ?>
                <div class="row">
                    <div class="col-md-9" style="margin-left:13%; margin-top:2%; position: relative;">
                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <form action="../controller/dispositivoController.php" method="post">

                                    <table class="table table-responsive">
                                        <tbody>
                                            <tr>
                                                <th scope="row"></th>
                                                <td colspan="2">
                                                    <p
                                                        style="font-family: 'Fjalla One', sans-serif; text-align:center;color:grey; font-size:18px">
                                                        Selecione o local do dispositivo</p>
                                                    <p style="text-align: center;">
                                                        <select name="canteiro" class="form-control"
                                                            required="required">
                                                            <option value="" disabled selected>Selecione a area para o
                                                                dispositivo</option>
                                                            <?php
                                                            $result = mysqli_query($conn, "SELECT tb_canteiro.idtb_canteiro,
                                                            tb_canteiro.local
                                                            from tb_canteiro inner join tb_usuario on
                                                            (tb_canteiro.idtb_canteiro = tb_usuario.idtb_usuario ) WHERE tb_canteiro.tb_usuario_idtb_usuario = '" . $_SESSION ['idtb_usuario'] . "'") or die();
                                                            if (mysqli_num_rows($result) > 0) {                                                                
                                                                while ($dado = mysqli_fetch_assoc($result)) {?>

                                                            <option value="<?=$dado['idtb_canteiro']?>">
                                                                <?php echo $dado['local']?></option>

                                                            <?php }}?>
                                                        </select></p>
                                                </td>

                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p
                                                        style="font-family: 'Fjalla One', sans-serif; text-align: center;color:grey; font-size:18px;">
                                                        Quantidade de Agua</p>
                                                    <p>
                                                        <div class="form-group"
                                                            style="font-family: 'Fjalla One', sans-serif; text-align: center;color:grey; font-size: 55px; font-weight:bold; ">
                                                            <?php
                                                        echo $qtdAgua = "0,00L";
                                                        echo "<input type='hidden' name ='qtdAgua' value='$qtdAgua'>";
                                                        ?>
                                                        </div>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p
                                                        style="font-family: 'Fjalla One', sans-serif; text-align: center;color:grey; font-size:18px;">
                                                        Data de Criação</p>
                                                    <p>
                                                        <div class="form-group"
                                                            style="font-family: 'Fjalla One', sans-serif; text-align: center;color:grey; font-size: 55px; font-weight:bold;">
                                                            <?php
                                                        echo $dataHoje = date("d/m/Y");
                                                        $dataCriacaoConv = preg_split('//', $dataHoje, -1, PREG_SPLIT_NO_EMPTY);
                                                        $dataCriacaoConv = $dataCriacaoConv[6] . $dataCriacaoConv[7] . $dataCriacaoConv[8] . $dataCriacaoConv[9] . "-" . $dataCriacaoConv[3] . $dataCriacaoConv[4] . "-" . $dataCriacaoConv[0] . $dataCriacaoConv[1];
                                                        echo "<input type='hidden' name ='dataCriacao' value=$dataCriacaoConv>";
                                                        ?>
                                                        </div>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p
                                                        style="font-family: 'Fjalla One', sans-serif; text-align: center;color:grey;  font-size:18px;">
                                                        Tempo de Atividade</p>
                                                    <p>
                                                        <div class="form-group"
                                                            style="font-family: 'Fjalla One', sans-serif; text-align: center;color:grey; font-size: 55px; font-weight:bold;">
                                                            <?php
                                                        echo $TempAtividade = "0:00:00";
                                                        echo "<input type='hidden' name ='tempAtividade' value='$TempAtividade'>";
                                                        ?>
                                                        </div>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p
                                                        style="font-family: 'Fjalla One', sans-serif; text-align: center;color:grey;  font-size:18px;">
                                                        Tempo Total de Atividade</p>
                                                    <p>
                                                        <div class="form-group"
                                                            style=" font-family: 'Fjalla One', sans-serif; text-align: center;color:grey; font-size: 55px; font-weight:bold;">
                                                            <?php
                                                        echo $TempTotalIrrigacao = "0:00:00";
                                                        echo "<input type='hidden' name ='tempTotalIrrigacao' value='$TempTotalIrrigacao'>";
                                                        ?>
                                                        </div>
                                                    </p>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <button type="submit" name="salvar" id='salvar'
                                                class="btn btn-block btn-info">Cadastrar</button>
                                        </div>
                                        <div class="col-xs-4">
                                            <button type="reset" name="cancelar"
                                                class="btn btn-block btn-warning">Cancelar</button>
                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>




            </section>
        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy;
                2014-2016 <a href="#">Almsaeed Studio</a>.</strong> All rights
            reserved.
        </footer>
    </div>
    <!--./wrapper -->
    <script src="../vberifyfile.js"></script>
    <!---->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../bower_components/raphael/raphael.min.js"></script>
    <script src="../bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- Paginação -->
    <script src="../bower_components/js/custom.js"></script>
    <!-- Paginação-->
    <script src="../bower_components/js/paginacao.js"></script>
</body>

</html>