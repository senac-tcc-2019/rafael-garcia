<?php
session_start();
include_once '../dao/conexao.php';

if ((!isset($_SESSION['username']) == true) && (!isset($_SESSION['senha']) == true)) {
    unset($_SESSION['username']);
    unset($_SESSION['senha']);

    header("location: http://localhost/irrigatech_git/rafael-garcia/index.php");
} else {
    $sql = "SELECT imagem FROM tb_usuario WHERE idtb_usuario= '" . $_SESSION['idtb_usuario'] . "'";
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    $row = mysqli_fetch_array($result);
    $imagem = $row['imagem'];
    if (empty($imagem)) {
        $imagem_caminho = "../dist/imagens/no-image-icon.png";
    } else {
        $imagem_caminho = "../dist/imagens/" . $imagem;
    }
}
////-------Recebe umidade e envia para o database------
//$valor é a variavel que esta sendo enviada pelo script c++
if (isset($_GET['valor'])) {
    $valor = $_GET['valor'];
//Recebe o valor da umidade e aramazena no banco de dados
    $result = mysqli_query($conn, "INSERT INTO `umidade` (`valor`) VALUES ('$valor');");
}
//recupera o valor registrado e insere no formulario para cadastrar a area
//$resultado = mysqli_query($conn, "SELECT * FROM umidade ORDER by id DESC LIMIT 1;");
//      mysqli_query("") or die(mysql_error());

//if (mysqli_num_rows($resultado) > 0) {
//   while ($dado = mysqli_fetch_assoc($resultado)) {
//        $umidade = $dado['status'];
//  }

?>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">




        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    </head>


    <style>

        .bStart{
            position: relative;
            text-align: center;
            margin-top: 4%;


        }

        .warning{
            margin-top: 6%;

        }
        .avisoSim{
            padding:5px; 
            font-size:18px; 
            text-align:center;
             background:#f44336;
             border-radius: 5px;
             color:white;
             
        }
        .avisoNao{
            padding:5px; 
            font-size:18px; 
            text-align:center;
            background:green;
            border-radius: 5px;
            color:white;
             
        }
        .avisoSim,.avisoNao p{
            margin-top:5px;
        }

        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            margin-left:38%;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }


    </style>
    <body class="hold-transition skin-blue sidebar-mini">



        <div class="wrapper">
            <header class="main-header">



                <!-- Logo -->
                <a href="homeView.php" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>I</b>T</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Irriga</b>Tech</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class="dropdown messages-menu">
                                <ul class="dropdown-menu">
                                    <!-- inner menu: contains the actual data -->                                                            
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <ul class="dropdown-menu">

                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                        </ul>
                                    </li>                                   
                                </ul>
                            </li>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <li class="dropdown tasks-menu">
                                <ul class="dropdown-menu">
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo $imagem_caminho; ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $_SESSION['username'] ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo $imagem_caminho; ?>" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo $_SESSION['username'] ?>

                                        </p>
                                    </li>
                                    <!-- Menu Body -->

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="../controller/logOutController.php" class="btn btn-default btn-flat">Sair</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo $imagem_caminho; ?>" style="width:80px;height:50px; border-radius: 50%;" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo$_SESSION['username']; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MENU PRINCIPAL</li>

                        <li><a href="homeView.php"><i class="fa fa-feed"></i> <span>Conexões</span></a></li>
                        <li><a href="dispositivoView.php"><i class="fa fa-plus-circle"></i> <span>Novo Dispositivo</span></a></li>
                        <li><a href="areaView.php"><i class="fa fa-plus-circle"></i> <span>Area de Monitoramento</span></a></li>
                        <li><a href="recursosView.php"><i class="fa fa-tint"></i> <span>Recursos</span></a></li>
                        <li><a href="agendamentoView.php?action=2"><i class="fa fa-calendar-o"></i> <span>Agendar</span></a></li>
                        <li><a href="relatorioView.php"><i class="fa fa-file-text"></i> <span>Relatório</span></a></li>
                        <li><a href="#"><i class="fa  fa-gears"></i> <span>Configurações</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->




                <section class="content-header">            
                    <ol class="breadcrumb">
                        <li><a href="homeView.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Nova Area de Monitoramento</li>
                    </ol>
                </section>     
                <!-- Main content -->
                <section class="content">
                    <br>
                <?php
                if (isset($_GET['valor'])) {
                    echo"<div class='avisoNao'><p>Sensor de umidade conectado.</p></div>";
                }else{
                    echo"<div class='avisoSim'><p>Seu sensor não está conectado, não será possivel cadastrar a área.</p></div>";
                }     
                ?>    
                    <div class="warning">
                        <div class="alert alert-info" role="alert">
                            <h4>Instruções de como coletar as informações do solo</h4>
                            <p>Para que sua área(Canteiro) seja cadastrada com sucesso siga as seguintes instruções:</p>
                            <ul>
                                <li>Introduza o sensor de umidade na local que será cadastrado.</li>
                                <li>Certifique-se que as duas astes estejam introduzidas totalmente no solo.</li>
                                <li>Pressione o botão <strong>"Coletar Informações"</strong> para dar inicio a coleta.</li>
                                <li>Após coletadas as informações você será direcionado para o formulario de cadastro para concluir o cadastro do local.</li>
                            </ul>
                        </div>
                    </div>


                    <div class ='bStart'>
                        <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#exampleModalScrollable">
                            Coletar Informações
                        </button>
                    </div>
                </section>

                <section class="content-fluid">


                    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalScrollableTitle" style="text-align: center;">Coletando Informações</h3>                                    
                                </div>
                                <div class="modal-body">                                    
                                    <?php
                                    echo"<form action='monitoramentoView.php' method='post'>";
                                    echo"<input type = 'hidden' name='umidade' value='$umidade'>";
                                    ?>

                                </div>

                                <div class = "modal-footer">
                                    <?php
                                    echo "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>";
                                    echo "<button type = 'submit' name='salvar' class = 'btn btn-primary'>Cadastrar Area</button>";
                                    echo"</form>";
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <footer class = "main-footer">
                <div class = "pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy;
                    2014-2016 <a href = "#">Almsaeed Studio</a>.</strong> All rights
                reserved.
            </footer>
        </div>
        <!--./wrapper -->
        <script src = "../vberifyfile.js"></script>
        <!---->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <!-- jQuery 3 -->
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);</script>
        <!-- Bootstrap 3.3.7 -->
        <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="../bower_components/raphael/raphael.min.js"></script>
        <script src="../bower_components/morris.js/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="../bower_components/moment/min/moment.min.js"></script>
        <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="../dist/js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
    </body>
</html>
<script>


            $(document).ready(function () {
                $('#myModal').modal('show');
            });
</script>




