<?php
session_start();
include_once '../dao/conexao.php';

if ((!isset($_SESSION['username']) == true) && (!isset($_SESSION['senha']) == true)) {
    unset($_SESSION['username']);
    unset($_SESSION['senha']);

    header("location: http://localhost/irrigatech_git/rafael-garcia/index.php");

} else {
    $sql = "SELECT imagem FROM tb_usuario WHERE idtb_usuario= '" . $_SESSION ['idtb_usuario'] . "'";
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    $row = mysqli_fetch_array($result);
    $imagem = $row['imagem'];
    if (empty($imagem)) {
        $imagem_caminho = "../dist/imagens/no-image-icon.png";
    } else {
        $imagem_caminho = "../dist/imagens/" . $imagem;
    }
}
?>
 <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE = edge">
        <title>IrrigaTech</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width = device-width, initial-scale = 1, maximum-scale = 1, user-scalable = no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href='../fullcalendar/css/core/main.min.css' rel='stylesheet' />
        <link href='../fullcalendar/css/daygrid/main.min.css' rel='stylesheet' />
        <link href='../fullcalendar/css/daygrid/main.min.css' rel='stylesheet' />
        <script src='../fullcalendar/js/core/main.min.js'></script>
        <script src='../fullcalendar/js/interaction/main.min.js'></script>
        <script src='../fullcalendar/js/daygrid/main.min.js'></script>
        <script src='../fullcalendar/js/core/locales/pt-br.js'></script>
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                var calendarEl = document.getElementById('calendar');

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    locale: 'pt-br',
                    plugins: ['interaction', 'dayGrid'],
                    //defaultDate: '2019-04-12',
                    editable: true,
                    eventLimit: true,
                    events: '../controller/agendamentoController.php',
                    extraParams: function () {
                        return {
                            cachebuster: new Date().valueOf()
                        };
                    },
                    eventClick: function(info) {
                         info.jsEvent.preventDefault();
                        $("#visualizar #id").text(info.event.id);
                        $("#visualizar #title").text(info.event.title);
                        $("#visualizar #start").text(info.event.start.toLocaleString());
                        $("#visualizar #end").text(info.event.end.toLocaleString());
                        $("#visualizar").modal('show');

                     
                        },
                        selectable:true,
                        select: function(info){
                            //alert('select '+ info.startStr +' to '+ info.endStr);
                         
                            $("#form").modal('show');
                        },
                        select:function(info){
                            if(info.start < new Date().valueOf()){
                                alert("Datas passadas não permitem agendamentos!");                          
                            return false;
                              
                            }else{
                                $("#form").modal('show');
                                $("#form #start").val(info.start.toLocaleString());
                            $("#form #end").val(info.end.toLocaleString());
                            }

                        },                       
                    });

                        $(document).ready(function(){
                             $('#addevent').on("submit",function(event){
                                 event.preventDefault();
                                 $.ajax({
                                     method:"POST",
                                     url:"../controller/teste.php",
                                     data: new FormData(this),
                                     contentType:false,
                                     processData:false,
                                     success: function(retorna){
                                         if(retorna['sit']){
                                             //$("#msg").html(retorna['msg']);
                                             location.reload();
                                         }else{
                                            $("#msg").html(retorna['msg']);
                                         }

                                     }
                                 })
                             });
                        });

                        $(document).ready(function(){
                             $('#removeevent').on("submit",function(event){
                                 event.preventDefault();
                                 $.ajax({
                                     method:"POST",
                                     url:"../controller/teste.php",
                                     data: new FormData(this),
                                     contentType:false,
                                     processData:false,
                                     success: function(retorna){
                                         if(retorna['sit']){
                                             //$("#msg").html(retorna['msg']);
                                             location.reload();
                                         }else{
                                            $("#msg").html(retorna['msg']);
                                         }

                                     }
                                 })
                             });
                        });
               

                calendar.render();
            });

        </script>
        <style>

            body {
                font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
                font-size: 14px;
            }

            #calendar {
                max-width: 900px;
                margin: 0 auto;
            }

        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="homeView.php" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>I</b>T</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Irriga</b>Tech</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class="dropdown messages-menu">
                                <ul class="dropdown-menu">                                   
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Reviewers
                                                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>                               
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <!-- Tasks: style can be found in dropdown.less -->
                            <li class="dropdown tasks-menu">
                                <ul class="dropdown-menu">

                                    <li>
                                        <ul class="menu">
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo $imagem_caminho; ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $_SESSION['username']; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo $imagem_caminho; ?>" class="img-circle" alt="User Image">

                                        <p>
                                            <?php echo$_SESSION['username']; ?>

                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="../controller/logOutController.php" class="btn btn-default btn-flat">Sair</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo $imagem_caminho; ?>" style="width:80px;height:50px; border-radius: 50%;" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo ucfirst($_SESSION['username']); ?> </p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MENU PRINCIPAL</li>

                        <li><a href="homeView.php"><i class="fa fa-feed"></i> <span>Conexões</span></a></li>
                        <li><a href="dispositivoView.php"><i class="fa fa-plus-circle"></i> <span>Novo Dispositivo</span></a></li>
                        <li><a href="areaView.php"><i class="fa fa-plus-circle"></i> <span>Area de Monitoramento</span></a></li>
                        <li><a href="recursosView.php"><i class="fa fa-tint"></i> <span>Recursos</span></a></li>
                        <li><a href="agendamentoView.php?action=2"><i class="fa fa-calendar-o"></i> <span>Agendar</span></a></li>
                        <li><a href="relatorioView.php"><i class="fa fa-file-text"></i> <span>Relatório</span></a></li>
                        <li><a href="#"><i class="fa  fa-gears"></i> <span>Configurações</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->

                <section class="content-header">
                    <h1>
                        Home
                        <small>Painel de Controle</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="homeView.php"><i class="fa fa-dashboard"></i>Home</a></li>
                        <li class="active">Home</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <?php 
                    if(isset($_SESSION['msg'])){
                        echo $_SESSION['msg'];
                        unset($_SESSION['msg']);
                    }
                    
                    ?>
                <div class="box">
                <div class="box-header with-border">
                <div id='calendar'></div>
        </div>
        </div>

<div class="modal fade" id="visualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Detalhes do Agendamento</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">     
      <dl class="row">
           <dt class="col-sm-3">ID do evento</dt>
           <dd class="col-sm-9" id="id"></dd>
           <dt class="col-sm-3">Local</dt>
           <dd class="col-sm-9" id="title"></dd>
           <dt class="col-sm-3">Inicio</dt>
           <dd class="col-sm-9" id="start"></dd>
           <dt class="col-sm-3">Fim</dt>
           <dd class="col-sm-9" id="end"></dd>
                </dl>
        </div>
      </div>
      <div class="modal-footer">   
      </div>
    </div>
  </div>
</div>

<!-- Formulario Agendamento!-->

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Agendar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <span id="msg"></span>
      <form id="addevent" method="POST" enctype="multipart/form-data">
  <div class="form-group">
    <label for="exampleFormControlSelect1">Local</label>
    <select class="form-control" name="local" id="exampleFormControlSelect1">
    <?php

     $result = mysqli_query($conn, "SELECT tb_canteiro.idtb_canteiro,tb_canteiro.local
                from tb_canteiro inner join tb_usuario on(tb_canteiro.idtb_canteiro = tb_usuario.idtb_usuario ) 
                WHERE tb_usuario.idtb_usuario = '" . $_SESSION ['idtb_usuario'] . "'") or die(mysqli_error());
                if (mysqli_num_rows($result) > 0) {
                    while ($dado = mysqli_fetch_assoc($result)) {?>
            <option value="<?=$dado['idtb_canteiro']?>"><?php echo $dado['local']?></option>                                                           
     <?php } }?>

    </select>

    <div class="form-group">
    <label for="exampleFormControlInput1">Data Inicio</label>
    <input type="text" class="form-control" id="start" name="start">
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlInput1">Data Termino</label>
    <input type="text" class="form-control" id="end" name="end">
  </div>
  <!-- Id do usuario!-->
  <div class="form-group">
    <input type="hidden" class="form-control" id="idUsuario" name="idUsuario" value="<?=$_SESSION ['idtb_usuario']?>">
  </div>
<!-- Id do Cor!-->
<div class="form-group">
    <input type="hidden" class="form-control" id="color" name="color" value="#FFD700">
  </div>
  <div class="form-group">
    <input type="hidden" class="form-control" id="acao" name="acao" value="1">
  </div>
  <button type ="submit" id="agendar" value="agendar"class="btn btn-success">Confirmar</button>
  </div>
</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

        </section>
        </div>

<footer class = "main-footer">
    <div class = "pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy;
        2014-2016 <a href = "#">Almsaeed Studio</a>.</strong> All rights
    reserved.
</footer>
</div>

    </body>
        <!--./wrapper -->
        <script src = "../vberifyfile.js"></script>
        <!---->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <!-- jQuery 3 -->
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
                                                                $.widget.bridge('uibutton', $.ui.button);</script>
        <!-- Bootstrap 3.3.7 -->
        <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="../bower_components/raphael/raphael.min.js"></script>
        <script src="../bower_components/morris.js/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="../dist/js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
    </body>
</html>
