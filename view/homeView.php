<?php
session_start();
include_once '../dao/conexao.php';
date_default_timezone_set("America/Sao_Paulo");
//Obtem o ultimo valor da umidade da API thingsSpeak
$link= "https://api.thingspeak.com/channels/842129/feeds.json?api_key=5GUU101Q7HSKNM4U&results=1";
$ch = curl_init($link);
curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
 
$array = json_decode($response, True);

$umidade = $umidade=$array['feeds'][0]['field1'] ;
$hora = $array['feeds'][0]['created_at'];


if ((!isset($_SESSION['username']) == true) && (!isset($_SESSION['senha']) == true)) {
    unset($_SESSION['username']);
    unset($_SESSION['senha']);

    header("location: http://localhost/irrigatech_git/rafael-garcia/index.php");
} else {
    $sql = "SELECT imagem FROM tb_usuario WHERE idtb_usuario= '" . $_SESSION['idtb_usuario'] . "'";
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    $row = mysqli_fetch_array($result);
    $imagem = $row['imagem'];
    if (empty($imagem)) {
        $imagem_caminho = "../dist/imagens/no-image-icon.png";
    } else {
        $imagem_caminho = "../dist/imagens/" . $imagem;
    }

}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE = edge">
    <title>IrrigaTech</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width = device-width, initial-scale = 1, maximum-scale = 1, user-scalable = no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!--estilo paginação-->
    <link rel="stylesheet" href="../bower_components/css/paginacao.css">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>


<style>
li {
    list-style: none;
}
</style>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="home.php" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>I</b>T</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Irriga</b>Tech</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="dist/img/user4-128x128.jpg" class="img-circle"
                                                        alt="User Image">
                                                </div>
                                                <h4>
                                                    Reviewers
                                                    <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <!-- Tasks: style can be found in dropdown.less -->
                        <li class="dropdown tasks-menu">
                            <ul class="dropdown-menu">

                                <li>
                                    <ul class="menu">
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo $imagem_caminho; ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo$_SESSION['username']; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php echo $imagem_caminho; ?>" class="img-circle" alt="User Image">

                                    <p>
                                        <?php echo$_SESSION['username']; ?>

                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="http://localhost/irrigatech_git/rafael-garcia/controller/logOutController.php"
                                            class="btn btn-default btn-flat">Sair</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo $imagem_caminho; ?>" style="width:80px;height:50px; border-radius: 50%;"
                            alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?php echo ucfirst($_SESSION['username']); ?> </p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                    class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MENU PRINCIPAL</li>

                    <li><a href="homeView.php"><i class="fa fa-feed"></i> <span>Conexões</span></a></li>
                    <li><a href="dispositivoView.php"><i class="fa fa-plus-circle"></i> <span>Novo
                                Dispositivo</span></a></li>
                    <li><a href="areaView.php"><i class="fa fa-plus-circle"></i> <span>Area de Monitoramento</span></a>
                    </li>
                    <li><a href="recursosView.php"><i class="fa fa-tint"></i> <span>Recursos</span></a></li>
                    <li><a href="agendamentoView.php?action=2"><i class="fa fa-calendar-o"></i> <span>Agendar</span></a>
                    </li>
                    <li><a href="relatorioView.php"><i class="fa fa-file-text"></i> <span>Relatório</span></a></li>
                    <li><a href="#"><i class="fa  fa-gears"></i> <span>Configurações</span></a></li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->

            <section class="content-header">
                <h1>
                    Home
                    <small>Home</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="homeView.php"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li class="active">Home</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <?php if (isset($_SESSION['mensagem'])): ?>
                <div class="warning">
                    <div class="alert alert-<?= $_SESSION['tipo_mensagem'] ?>">
                        <?php
                                echo$_SESSION['mensagem'];
                                unset($_SESSION['mensagem']);
                                ?>
                    </div>
                </div>
                <?php endif ?>

                <!-- Tabela de Dispositivos Cadastrados INICIO -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Dispositivos Cadastrados</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <table class="table table-bordered">

                            <tr>

                                <th style="text-align:center">Dispositivos</th>
                                <th style="text-align:center">Status</th>
                                <th style="text-align:center">Ultima Atividade</th>
                                <th style="text-align:center">Ações</th>
                            </tr>
                            <tr>
                                <?php
                                        $result = mysqli_query($conn,  "SELECT tb_canteiro.local,
                                        tb_usuario_idtb_usuario,
                                        tb_dispositivo.iddispositivo,
                                        tb_dispositivo.status,
                                        tb_dispositivo.DtCriacao
                                        FROM tb_canteiro inner join tb_usuario on (tb_usuario.idtb_usuario = tb_canteiro.tb_usuario_idtb_usuario) inner join
                                        tb_dispositivo on (tb_canteiro_idtb_canteiro = tb_canteiro.tb_usuario_idtb_usuario )
                                        where tb_canteiro.tb_usuario_idtb_usuario = '" . $_SESSION ['idtb_usuario'] . "'") or die(mysqli_error($conn));

                                        if (mysqli_num_rows($result) > 0) {
                                            while ($dado = mysqli_fetch_assoc($result)) {
                                         
                                                ?>

                                <td style="text-align:center"><a
                                        href="detalheView.php?id=<?php echo $dado['iddispositivo']; ?>">
                                        <?php echo $dado['local']; ?></a></td>
                                <td  style="text-align:center"><div id="msg" style="color:red; font-weight:bold;">Desaligado</div></td>
                                <td style="text-align:center"> <?php echo $dado['DtCriacao'] ?></td>
                                <td style="text-align:center">
                                    <?php echo "<button type='button' class='btn btn-success btn-sm' onclick='ledState(1)'>Ligar</button>&nbsp&nbsp&nbsp&nbsp<button type='button' class='btn btn-warning btn-sm' onclick='ledState(0)'>Desligar</button>&nbsp&nbsp&nbsp&nbsp<button type='button' class='btn btn-danger btn-sm'>Configurações</button>"; ?>
                                </td>
                            </tr>
                            <?php
                                        
                                   } } else {
                                        ?>
                            <div style='color:red; text-align:center;'>
                                <p>Não existem Dispositivos Cadastrados</p>
                                <p>* Certifique-se de cadastrar Areas de Irrigação antes de cadastrar dispositivos</p>
                                <div style="margin-bottom:8px;"><a href='dispositivoView.php' class='btn btn-info'
                                        role='button'>Cadastrar Dispositivo</a></div>
                            </div>

                            <?php } ?>
                        </table>
                    </div>
                </div>
                <!-- Tabela de Dispositivos Cadastrados FIM -->

                <!-- Tabela de Dispositivos Areas de Irrigação INICIO-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <i class="fa fa-tree"></i>

                                <h3 class="box-title">Areas de Irrigação</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th style="text-align:center">Local</th>
                                            <th style="text-align:center">Umidade</th>
                                            <th style="text-align:center">Data Atualização</th>
                           
                                        </tr>
                                        <tr>
                                            <?php
                                              $query = "SELECT tb_canteiro.idtb_canteiro,
                                              tb_canteiro.local,
                                              tb_usuario_idtb_usuario,
                                              tb_dispositivo.iddispositivo,
                                              tb_dispositivo.DtCriacao FROM tb_canteiro inner join tb_usuario on
                                              (tb_usuario.idtb_usuario = tb_canteiro.tb_usuario_idtb_usuario) inner join
                                              tb_dispositivo on (tb_canteiro_idtb_canteiro = tb_canteiro.tb_usuario_idtb_usuario )
                                              where tb_canteiro.tb_usuario_idtb_usuario = '".$_SESSION['idtb_usuario']."'";
                                            
                                                    $result = mysqli_query($conn, $query) or die(mysqli_error($connect));
                                            
                                                     while($row =mysqli_fetch_array($result))
                                                    { ?>

                                           
                                            <td style="text-align:center"><?php echo $row['idtb_canteiro'];?></td>
                                            <td style="text-align:center"> <a href="infoView.php?id=<?php echo $row['idtb_canteiro']; ?>"> <?php echo $row['local']; ?></a></td>
                                            <td style="text-align:center"> <?php echo $umidade. " %";?></td>
                                            <td style="text-align:center"> <?php echo $data = date ('d-m-Y',strtotime($hora));?></td>
                                    
                                        </tr>
                                        <?php }?>
                                    </table>
                                </div>


                                <!-- /.box-body -->

                            </div>

                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                    <!-- Tabela de Dispositivos Areas de Irrigação FIM-->


                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <i class="fa fa-calendar-times-o "></i>
                                <h3 class="box-title">Irrigações Agendadas</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                        $sql = " SELECT 
                                        tb_canteiro.local,
                                        tb_agendamento.idtb_agendamento,
                                        tb_agendamento.cor,
                                        tb_agendamento.dataInicio,
                                        tb_agendamento.dataTermino
                                        from tb_agendamento inner join tb_usuario on 
                                        (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario) 
                                        inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro )where tb_agendamento.tb_usuario_idtb_usuario = '" . $_SESSION ['idtb_usuario'] . "'";
                                        $result = mysqli_query($conn,$sql ) or die(mysqli_error($conn));
                                        if (mysqli_num_rows($result) > 0) {
                                            while ($dado = mysqli_fetch_assoc($result)) {
                                                echo "<div class='show_paginacao'>";
                                                $id = $dado['idtb_agendamento'];
                                                ?>
                                <div class="callout callout-default">

                                    <h4><?php echo"Local: " . $dado['local']; ?></h4>
                                    <ul>
                                        <li><i class="fa fa-clock-o">
                                            </i><?php echo "  " . $dado['dataInicio'] . " até" . " " . $dado['dataTermino']; ?>
                                        </li>
                                        <li><i class="fa  fa-calendar-check-o ">
                                            </i><?php echo "  " . $dado['dataInicio'] . " até" . " " . $dado['dataTermino']; ?>
                                        </li>

                                    </ul>
                                    <div class="row">
                                        <div class="col-xs-4">

                                        </div>

                                        <div class="col-xs-4">
                                            <a href='agendamentoView.php?action=1&id=<?= $id; ?>'
                                                style='text-decoration: none;' class='btn btn-block btn-danger'
                                                role='button'>Alterar</a>

                                        </div>
                                        <div class="col-xs-4">
                                            <a href="agendamentoView.php?id=<?= $id; ?>&action=0"
                                                style='text-decoration: none;'
                                                onclick="return confirm('Excluir Agendamento? ')"
                                                class='btn btn-block btn-danger' role='button'>Excluir</a>
                                        </div>

                                    </div>
                                </div>
                                <?php
                                                echo"</div>";
                                            } } else {
                                            echo"<div style = 'color:red; text-align:center;'><p>Não exitem Agendamentos</p>";
                                            echo"<div style = 'margin-left:2%;'><a href='agendamentoView.php' class='btn btn-info' role='button'>Agendar</a></div>";
                                         }
                                        ?>
                                <div class="pagination">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy;
                2014-2016 <a href="#">Almsaeed Studio</a>.</strong> All rights
            reserved.
        </footer>
    </div>
    <!--./wrapper -->
    <script src="../vberifyfile.js"></script>
    <!---->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../bower_components/raphael/raphael.min.js"></script>
    <script src="../bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- Paginação -->
    <script src="../bower_components/js/custom.js"></script>
    <!-- Paginação-->
    <script src="../bower_components/js/paginacao.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.min.js"></script>
    <script>
        
// Create a client instance
// ############# ATTENTION: Enter Your MQTT TLS Port and host######## Supports only TLS Port
  client = new Paho.MQTT.Client("m15.cloudmqtt.com", 30936 ,"web_" + parseInt(Math.random() * 100, 10));

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

//############# ATTENTION: Enter Your MQTT user and password details ########  
var options = {
  useSSL: true,
  userName: "jfepzzia",
  password: "rKNATP5qAOY_",
  onSuccess:onConnect,
  onFailure:doFail
}

// connect the client
client.connect(options);

// called when the client connects
function onConnect() {
  // Once a connection has been made, make a subscription and send a message.
  console.log("onConnect");
  client.subscribe("esp/test");
  message = new Paho.MQTT.Message("Hello CloudMQTT");
  message.destinationName = "esp/test";
  client.send(message);
}

function ledState(state) {
  if(state == 1) { 
        var div = document.getElementById('msg').textContent="Ativado";

        message = new Paho.MQTT.Message("#on"); 

  }
  if(state == 0) { 

    var div = document.getElementById('msg').textContent="Desativado";
    
 
  
      message = new Paho.MQTT.Message("#off");
      


       }
  message.destinationName = "esp/test";
  client.send(message);
}

function doFail(e){
  console.log(e);
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}

// called when a message arrives
function onMessageArrived(message) {
  console.log("onMessageArrived:"+message.payloadString);
}


</script>
<script>
  function myFunction() {
      <?php
      if($umidade > 90){?>
        confirm("Tem certeza que deseja irrigar?");
      <?php }?> 
}
</script>
</body>

</html>