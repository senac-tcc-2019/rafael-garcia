<?php
echo "Relatorio";
session_start();
require('pdf/fpdf.php');

$servername = "localhost";
$database = "u228397656_irrig";
$username = "u228397656_root";
$password = "123456";

$conn = mysqli_connect($servername, $username, $password, $database);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

class myPDF extends FPDF{

    function header(){
        $dataHoje = date("d/m/Y") ;
        
        $this->SetFont('Arial','B',14);
        $this->Cell(276,5,'Informacoes Mensal de Monitoramento',0,0,'C');
        $this->Ln();
        $this->SetFont('Times','',12);
        $this->Cell(276,5,$dataHoje,0,0,'C');
        $this->Ln(20);
    }

    function Segundoheader(){
        $this->Ln();
        $this->SetFont('Arial','B',14);
        $this->Cell(276,5,'Informacoes de Agendamento',0,0,'L');
        $this->Ln(10);
    }

    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function footerTableAgendamento(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
   /* function HeaderTable(){
        $this->SetFont('Times','',12);
        $this->Cell(20,10,'COD',1,0,'C');
        $this->Cell(40,10,'DATA',1,0,'C');
        $this->Cell(40,10,'Usuario',1,0,'C');
        $this->Cell(40,10,'Dispositivos',1,0,'C');
        $this->Cell(60,10,'Total Irrigacoes',1,0,'C');
        $this->Cell(60,10,'Tempo de Irrigacao',1,0,'C');
        $this->Ln();
    }*/
    function headerTableAgendamento(){
        $this->SetFont('Times','',12);
        $this->Cell(20,10,'COD',1,0,'C');
        $this->Cell(40,10,'Usuario',1,0,'C');
        $this->Cell(40,10,'Local',1,0,'C');
        $this->Cell(40,10,'Data Inicio',1,0,'C');
        $this->Cell(40,10,'Hora Inicio',1,0,'C');
        $this->Cell(40,10,'Data Termino',1,0,'C');
        $this->Cell(40,10,'Hora Termino',1,0,'C');
        $this->Ln();
    }



    /*function viewTable($conn){

        $this->SetFont('Times','',12);
        $sql = "SELECT tb_usuario.username,
        COUNT(tb_agendamento.idtb_agendamento) as agendamento
         from tb_agendamento inner join tb_usuario on 
         (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario) 
         inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro )where tb_agendamento.tb_usuario_idtb_usuario = 1;                                      ";
        $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
        if (mysqli_num_rows($result) > 0) {
            while ($dado = mysqli_fetch_assoc($result)) {
                $data = $dado['dataInicio'];

                $this->Cell(20,10,$dado['username'],1,0,'C');
                $this->Cell(40,10,$dado['agendamento'],1,0,'C');
                $this->Cell(40,10,$dado['idtb_agendamento'],1,0,'C');
                $this->Cell(40,10,$dado['idtb_agendamento'],1,0,'C');
                $this->Cell(60,10,$dado['idtb_agendamento'],1,0,'C');
                $this->Cell(60,10,date('d/m/Y', strtotime($data)),1,0,'C');
                $this->Ln();


    }
        }

        
}*/
function viewTableAgendamento($conn){

    $this->SetFont('Times','',12);
    $sql = "select 
    tb_agendamento.idtb_agendamento,
    tb_usuario.username,
    tb_canteiro.idtb_canteiro,
    tb_canteiro.local,
    tb_agendamento.dataInicio,
    tb_agendamento.dataTermino
    from tb_agendamento inner join tb_usuario on 
    (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario)
    inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro)
    where  tb_agendamento.tb_usuario_idtb_usuario = 1 and tb_canteiro.tb_usuario_idtb_usuario = '" . $_SESSION['idtb_usuario'] . "'";
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    if (mysqli_num_rows($result) > 0) {
        while ($dado = mysqli_fetch_assoc($result)) {
            $dataI = $dado['dataInicio'];    
            $dataT= $dado['dataTermino'];        

            $this->Cell(20,10,$dado['idtb_agendamento'],1,0,'C');
            $this->Cell(40,10,$dado['username'],1,0,'C');
            $this->Cell(40,10,$dado['local'],1,0,'C');
            $this->Cell(40,10,date('d/m/Y', strtotime($dataI)),1,0,'C');
            $this->Cell(40,10,date('H:m:s', strtotime($dataI)),1,0,'C');
            $this->Cell(40,10,date('d/m/Y', strtotime($dataT)),1,0,'C');
            $this->Cell(40,10,date('H:m:s', strtotime($dataT)),1,0,'C');
            $this->Ln();


}
    }
}
function dadosUmidade($conn){
    $this->SetFont('Times','',12);
    $sql = "SELECT AVG(valor) as media FROM umidade where tb_canteiro_idtb_canteiro = '" . $_SESSION['idtb_usuario'] . "'";
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    if (mysqli_num_rows($result) > 0) {
        while ($dado = mysqli_fetch_assoc($result)) {          

            $this->Ln();
            $this->SetFont('Arial','',14);
            $this->Cell(276,5,"Informacoes Gerais",0,0,'C');
            $this->Ln(10);
            $this->Cell(276,5,"Media de Umidade ............................................................".number_format($dado['media'], 1)."%",0,0,'L');
            $this->Ln(10);
}
}
}
function totalAgendamentos($conn){
    $this->SetFont('Times','',12);
    $sql = "SELECT 
    COUNT(tb_agendamento.idtb_agendamento) as agendamento
     from tb_agendamento inner join tb_usuario on 
     (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario) 
     inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro )where tb_agendamento.tb_usuario_idtb_usuario = '" . $_SESSION['idtb_usuario'] . "'";                                  
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    if (mysqli_num_rows($result) > 0) {
        while ($dado = mysqli_fetch_assoc($result)) {          
         
            $this->Ln(3);
            $this->SetFont('Arial','',14);
            $this->Cell(276,5,"Total de Agendamentos...................................................... ".$dado['agendamento'],0,0,'L');
            $this->Ln(10);
}
}
}
function totalDispositivos($conn){
    $this->SetFont('Times','',12);
    $sql ="SELECT tb_canteiro.local,
    tb_usuario_idtb_usuario,
    COUNT(tb_dispositivo.iddispositivo) as dispositivos,
    SUM(tb_dispositivo.qtdAgua) as consumo,
    AVG(tb_dispositivo.qtdAgua) as media,
    SUM(tb_dispositivo.TempAtividade) as atividade,
    tb_dispositivo.status,
    tb_dispositivo.DtCriacao
    FROM tb_canteiro inner join tb_usuario on (tb_usuario.idtb_usuario = tb_canteiro.tb_usuario_idtb_usuario) inner join
    tb_dispositivo on (tb_canteiro_idtb_canteiro = tb_canteiro.tb_usuario_idtb_usuario )
    where tb_canteiro.tb_usuario_idtb_usuario = '" . $_SESSION['idtb_usuario'] . "'";                                    
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    if (mysqli_num_rows($result) > 0) {
        while ($dado = mysqli_fetch_assoc($result)) {          
         
            $this->Ln(3);
            $this->SetFont('Arial','',14);
            $this->Cell(276,5,"Total de Dispositvivos......................................................... ".$dado['dispositivos'],0,0,'L');
            $this->Ln(10);
            $this->Ln(3);
            $this->SetFont('Arial','',14);
            $this->Cell(276,5,"Consumo Mensal de Agua ..................................................".$dado['consumo']." Litros",0,0,'L');
            $this->Ln(10);
            $this->Ln(3);
            $this->SetFont('Arial','',14);
            $this->Cell(276,5,"Media de Consumo...............................................................".number_format($dado['media'], 1)."%",0,0,'L');
            $this->Ln(10);
}
}
}
function totalHoras($conn){
    $this->SetFont('Times','',12);
    $sql ="SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( TempAtividade ) ) ) AS total_horas FROM tb_dispositivo inner join tb_canteiro on
    (tb_canteiro.idtb_canteiro = tb_dispositivo.iddispositivo) inner join tb_usuario on(tb_canteiro.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario)
    where tb_canteiro.tb_usuario_idtb_usuario = '" . $_SESSION['idtb_usuario'] . "'";
                                          
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    if (mysqli_num_rows($result) > 0) {
        while ($dado = mysqli_fetch_assoc($result)) {          
         
            $this->Ln(3);
            $this->SetFont('Arial','',14);
            $this->Cell(276,5,"Total de Horas.................................................................... ".$dado['total_horas'],0,0,'L');
            $this->Ln(10);
}
}
}
}
$pdf = new myPDF();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
//$pdf->headerTable();
//$pdf->viewTable($conn);
$pdf->Segundoheader();
$pdf->headerTableAgendamento();
$pdf->viewTableAgendamento($conn);
$pdf->dadosUmidade($conn);
$pdf->totalAgendamentos($conn);
$pdf->totalDispositivos($conn);
$pdf->totalHoras($conn);
$pdf->Output();
?>

