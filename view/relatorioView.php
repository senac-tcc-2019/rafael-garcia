<?php
session_start();
include_once '../dao/conexao.php';

if ((!isset($_SESSION['username']) == true) && (!isset($_SESSION['senha']) == true)) {
    unset($_SESSION['username']);
    unset($_SESSION['senha']);

    header("location: http://localhost/irrigatech_git/rafael-garcia/index.php");
} else {
    $sql = "SELECT imagem FROM tb_usuario WHERE idtb_usuario= '" . $_SESSION['idtb_usuario'] . "'";
    $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
    $row = mysqli_fetch_array($result);
    $imagem = $row['imagem'];
    if (empty($imagem)) {
        $imagem_caminho = "../dist/imagens/no-image-icon.png";
    } else {
        $imagem_caminho = "../dist/imagens/" . $imagem;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE = edge">
        <title>IrrigaTech</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width = device-width, initial-scale = 1, maximum-scale = 1, user-scalable = no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <!-- Morris chart -->
       <!-- <link rel="stylesheet" href="../bower_components/morris.js/morris.css"> -->
        <!-- jvectormap -->
        <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">




        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    </head>


<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
var data = new google.visualization.arrayToDataTable([
 ['data','Umidade'],
 <?php
 while($row =mysqli_fetch_array($result))
 {
     $data = date('d-m-Y',strtotime($row["data"]));
     echo"['".$data."',".$row["valor"]."],";
 }
 ?>
 ]);

var options = {
title:'Percentual de Umidade Hoje',
legend:{position:'bottom'},
chartArea:{width:'60%', height:'40%'}
};

var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

chart.draw(data, options);
}
</script>


    <style>
        li{
            list-style:none;
        }
    </style>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="home.php" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>I</b>T</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Irriga</b>Tech</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class="dropdown messages-menu">
                                <ul class="dropdown-menu">                                   
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Reviewers
                                                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>                               
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <!-- Tasks: style can be found in dropdown.less -->
                            <li class="dropdown tasks-menu">
                                <ul class="dropdown-menu">

                                    <li>
                                        <ul class="menu">
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo $imagem_caminho; ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Alexander Pierce</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo $imagem_caminho; ?>" class="img-circle" alt="User Image">

                                        <p>
                                            <?php echo$_SESSION['nome']; ?>

                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="http://localhost/irrigatech_git/rafael-garcia/controller/logOutController.php" class="btn btn-default btn-flat">Sair</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo $imagem_caminho; ?>" style="width:80px;height:50px; border-radius: 50%;" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo ucfirst($_SESSION['nome']); ?> </p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MENU PRINCIPAL</li>

                        <li><a href="homeView.php"><i class="fa fa-feed"></i> <span>Conexões</span></a></li>
                        <li><a href="dispositivoView.php"><i class="fa fa-plus-circle"></i> <span>Novo Dispositivo</span></a></li>
                        <li><a href="areaView.php"><i class="fa fa-plus-circle"></i> <span>Area de Monitoramento</span></a></li>
                        <li><a href="recursosView.php"><i class="fa fa-tint"></i> <span>Recursos</span></a></li>
                        <li><a href="agendamentoView.php?action=2"><i class="fa fa-calendar-o"></i> <span>Agendar</span></a></li>
                        <li><a href="relatorioView.php"><i class="fa fa-file-text"></i> <span>Relatório</span></a></li>
                        <li><a href="#"><i class="fa  fa-gears"></i> <span>Configurações</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->

                <section class="content-header">
                    <h1>
                        Home
                        <small>Relatório</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="homeView.php"><i class="fa fa-dashboard"></i>Home</a></li>
                        <li class="active">Relatório</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content-fluid">

                <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Relatório de Atividades</h3>
              <div class="box-tools">
              <a href="gerarRelatorio.php" class="btn btn-primary">Gerar PDF</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Descrição</th>
                  <th></th>
                  <th style="width: 40px">Dados</th>
                </tr>
                <?php
                $sql = "SELECT tb_usuario.username,
                COUNT(tb_dispositivo.iddispositivo) AS dispositivos,
                COUNT(tb_agendamento.idtb_agendamento) AS AGENDAMENTOS,
                SUM(tb_dispositivo.QtdAgua) AS QtdAgua,
                SUM(TempTotalirrigacao ) AS tempo
                from tb_canteiro inner join tb_usuario on
                           (tb_usuario.idtb_usuario = tb_canteiro.tb_usuario_idtb_usuario) inner join 
                           tb_dispositivo on (tb_dispositivo.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro) 
                           inner join tb_agendamento on(tb_agendamento.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro) where tb_canteiro.tb_usuario_idtb_usuario = '" . $_SESSION['idtb_usuario'] . "'" ;
           
        
                
                $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));

                while($dado = mysqli_fetch_assoc($result)){                         
                ?>
                <tr>
                  <td>1.</td>
                  <td>Usuário</td>
                  <td>
                  </td>
                  <td><p><?php echo $_SESSION['username']; ?></p></td>
                </tr>
                <?php
                $sql = "SELECT tb_canteiro.local,
                tb_usuario_idtb_usuario,
                COUNT(tb_dispositivo.iddispositivo) as dispositivos,
                tb_dispositivo.status,
                tb_dispositivo.DtCriacao
                FROM tb_canteiro inner join tb_usuario on (tb_usuario.idtb_usuario = tb_canteiro.tb_usuario_idtb_usuario) inner join
                tb_dispositivo on (tb_canteiro_idtb_canteiro = tb_canteiro.tb_usuario_idtb_usuario )
                where tb_canteiro.tb_usuario_idtb_usuario ='" . $_SESSION['idtb_usuario'] . "'";
           
        
                
                $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));

                while($dado = mysqli_fetch_assoc($result)){                         
                ?>                
                <tr>
                  <td>2.</td>
                  <td>Dispositivos Cadastrados</td>
                  <td>
                  </td>
                  <td><p><?php echo $dado['dispositivos']; ?></p></td>
                </tr>
                <?php }?>
                <?php
                $sql = "SELECT 
                COUNT(tb_agendamento.idtb_agendamento) as agendamento
                 from tb_agendamento inner join tb_usuario on 
                 (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario) 
                 inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro )where tb_agendamento.tb_usuario_idtb_usuario ='" . $_SESSION['idtb_usuario'] . "'";
           
        
                
                $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));

                while($dado = mysqli_fetch_assoc($result)){  
                ?>
                <tr>
                  <td>3.</td>
                  <td>Agendamentos</td>
                  <td>
                  </td>
                  <td><p><?php echo $dado['agendamento']; ?></p></td>
                </tr>
                <?php }?>
                <?php
                $sql = "SELECT tb_canteiro.local,
                tb_usuario_idtb_usuario,
                COUNT(tb_dispositivo.iddispositivo) as dispositivos,
                SUM(tb_dispositivo.qtdAgua) as consumo,
                AVG(tb_dispositivo.qtdAgua) as media,
                SUM(tb_dispositivo.TempAtividade) as atividade,
                tb_dispositivo.status,
                tb_dispositivo.DtCriacao
                FROM tb_canteiro inner join tb_usuario on (tb_usuario.idtb_usuario = tb_canteiro.tb_usuario_idtb_usuario) inner join
                tb_dispositivo on (tb_canteiro_idtb_canteiro = tb_canteiro.tb_usuario_idtb_usuario )
                where tb_canteiro.tb_usuario_idtb_usuario ='" . $_SESSION['idtb_usuario'] . "'";
           
        
                
                $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));

                while($dado = mysqli_fetch_assoc($result)){  
                ?>
                <tr>
                  <td>4.</td>
                  <td>Consumo mensal de água</td>
                  <td>
                  </td>
                  <td><p><?php echo number_format($dado['consumo'], 2)."L"?></p>
                </td>
                </tr>
                <?php
                $sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( TempAtividade ) ) ) AS total_horas FROM tb_dispositivo inner join tb_canteiro on
                (tb_canteiro.idtb_canteiro = tb_dispositivo.iddispositivo) inner join tb_usuario on(tb_canteiro.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario)
                where tb_canteiro.tb_usuario_idtb_usuario = '" . $_SESSION['idtb_usuario'] . "'";
                        
                $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));

                while($dado = mysqli_fetch_assoc($result)){  
                ?>
                <tr>
                <td>5.</td>
                  <td>Total de Horas de Irrigação</td>
                  <td>
                  </td>
                  <td><p><?php echo $dado['total_horas']?></p>
                </td>
                </tr>
                <?php } } }?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>  
          <div class="row">
          <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Informações de Agendamento</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>COD</th>
                  <th>Usuario</th>
                  <th>Local</th>
                  <th>Data Inicio</th>
                  <th>Hora Inicio</th>
                  <th>Data Inicio</th>
                  <th>Hora Inicio</th>
                </tr>
                <?php
                  $sql = "select 
                  tb_agendamento.idtb_agendamento,
                  tb_usuario.username,
                  tb_canteiro.idtb_canteiro,
                  tb_canteiro.local,
                  tb_agendamento.dataInicio,
                  tb_agendamento.dataTermino
                  from tb_agendamento inner join tb_usuario on 
                  (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario)
                  inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro)
                  where  tb_agendamento.tb_usuario_idtb_usuario ='" . $_SESSION['idtb_usuario'] . "'";
             
          
                  
                  $result = mysqli_query($conn, $sql)or die(mysqli_error($conn));
  
                  while($dado = mysqli_fetch_assoc($result)){ 

                    $dataI = $dado['dataInicio'];    
                    $dataT= $dado['dataTermino']; 

                ?>
                <tr>
                  <td><?php echo $dado['idtb_agendamento'] ?></td>
                  <td><?php echo $dado['username'] ?></td>
                  <td><?php echo $dado['local'] ?></td>
                  <td><?php echo date('d/m/Y', strtotime($dataI))?></td>
                  <td><?php echo date('H:m:s', strtotime($dataI)) ?></td>
                  <td><?php echo date('d/m/Y', strtotime($dataT)) ?></td>
                  <td><?php echo date('H:m:s', strtotime($dataT))?></td>
                </tr>
                <tr>

                </tr>
                  <?php }?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
</div>        
        </section>

        
    </div>
    <footer class = "main-footer">
                <div class = "pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy;
                    2014-2016 <a href = "#">Almsaeed Studio</a>.</strong> All rights
                reserved.
            </footer>
        </div>
        <!--./wrapper -->
        <script src = "../vberifyfile.js"></script>
        <!---->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <!-- jQuery 3 -->
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);</script>
        <!-- Bootstrap 3.3.7 -->
        <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
       <script src="../bower_components/raphael/raphael.min.js"></script> 
       <script src="../bower_components/morris.js/morris.min.js"></script> 
       <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- Sparkline -->
        <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="../bower_components/moment/min/moment.min.js"></script>
        <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="../dist/js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
    </body>
</html>



