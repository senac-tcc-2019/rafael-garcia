#include <ESP8266WiFi.h>
#include <PubSubClient.h>
 
#define LED 5
#define SSID_REDE "Rafael Garcia"
#define SENHA_REDE "isabella23"
#define INTERVALO_ENVIO_THINGSPEAK 30000
#define ID_MQTT "jfepzzia";

//Configuração ThingSpeak
// Configurações Cloud MQTT

char EnderecoAPI[]= "api.thingspeak.com";
const char * BROKER_MQTT = "m15.cloudmqtt.com";
int BROKER_PORT = 10936;
const char* mqttPassword = "rKNATP5qAOY_"; 
const char* mqttUser = "jfepzzia";
String ChaveEscritaThingSpeak = "L9WL24O7T2DBW1L8";
long lastConnectionTime;
long lastMQTTSendTime;

WiFiClient client;
WiFiClient ClientMQTT;
PubSubClient MQTT(ClientMQTT);

//Funções


void EnviarInformacoesThingSpeak(String StringDados)
{
  if(client.connect(EnderecoAPI, 80))
  {
        client.print("POST /update HTTP/1.1\n");
        client.print("Host: api.thingspeak.com\n");
        client.print("Connection: close\n");
        client.print("X-THINGSPEAKAPIKEY: "+ChaveEscritaThingSpeak+"\n");
        client.print("Content-Type: application/x-www-form-urlencoded\n");
        client.print("Content-Length: ");
        client.print(StringDados.length());
        client.print("\n\n");
        client.print(StringDados);

        lastConnectionTime = millis();
        Serial.println("Informações Enviadas ao ThingSpeak");
  
  }
}

void initWiFi()
{
  delay(1000);
  Serial.println("------Conexao WiFi-------");
  Serial.print("Conectando-se na rede");
  Serial.println(SSID_REDE);
  Serial.println("Aguarde...");

  reconnectWiFi();
}

void reconnectMQTT()
{
  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  if (MQTT.connect("ESP8266", mqttUser, mqttPassword )) {

        Serial.println("MQTT conectado.");  
        MQTT.setCallback(mqtt_callback);
        MQTT.publish("esp/test", "Hello from ESP8266");
        MQTT.subscribe("esp/test");
        
    }else{
      Serial.println("Não foi possivel connectar ao broker");
      
      Serial.println( MQTT.state());
      delay(2000);
    }
}
void reconnectWiFi()
{
    if (WiFi.status() == WL_CONNECTED)
        return;       
    WiFi.begin(SSID_REDE, SENHA_REDE); 
    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(1000);
        Serial.print(".");
    }
     
    Serial.println();
    Serial.print("Conectado com sucesso na rede ");
    Serial.print(SSID_REDE);
    Serial.println("IP obtido: ");
    Serial.println(WiFi.localIP());
}


void verificaConexaoWiFiEMQTT(void)
{
  if(!MQTT.connected())
  {
    reconnectMQTT();
    reconnectWiFi();
  }
  
}

float fazLeituraUmidade(void)
{
  int ValorADC;
  float UmidadePercentual;

  ValorADC = analogRead(0);
  Serial.print("[Leitura ADC] ");
  Serial.println(ValorADC);
  UmidadePercentual = 100 *((978-(float)ValorADC)/978);
  Serial.print("[Umidade Percentual]");
  Serial.print(UmidadePercentual);
  Serial.println("%");

  return UmidadePercentual;
}

void setup()
{
  Serial.begin(115200);
  lastConnectionTime = 0;
  initWiFi();
  pinMode(LED,OUTPUT);
  Serial.println("Planta IoT com ESP8266 NodeMCU");
  MQTT.setServer( BROKER_MQTT,BROKER_PORT);

}

void mqtt_callback(char* topic, byte* payload, unsigned int length)
{
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
 
  String message;
  Serial.println(message);
  for (int i = 0; i < length; i++) {
    message = message + (char)payload[i]; 
  }
   Serial.print(message);
  if(message == "#on") {digitalWrite(LED,LOW);}   //LED ligado  
  if(message == "#off") {digitalWrite(LED,HIGH);} //LED desligado
 
  Serial.println();
  Serial.println("-----------------------"); 
}

void loop()
{
  MQTT.loop();
  float UmidadePercentualLida;
  int UmidadePercentualTruncada;
  char FieldUmidade[11];
 

  verificaConexaoWiFiEMQTT();

  if(client.connected())
  {
    client.stop();
    Serial.println("-Desconectado do thingspeak");
    Serial.println();
  }
  UmidadePercentualLida = fazLeituraUmidade();
  UmidadePercentualTruncada = (int)UmidadePercentualLida;

  if(!client.connected() && ((millis() - lastConnectionTime) > INTERVALO_ENVIO_THINGSPEAK ))
  {
    sprintf(FieldUmidade,"field1=%d",UmidadePercentualTruncada);
    EnviarInformacoesThingSpeak(FieldUmidade);

  }
delay(1000);
}
