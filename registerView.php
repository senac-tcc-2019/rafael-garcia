<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Registration Page</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition register-page">
    <div class="register-box" style="margin-top:0px;">

        <div class="register-logo">
            <a href="#"><b>Irriga</b>Tech</a>
        </div>

        <div class="register-box-body">
            <form action="controller/loginController.php" method="post" enctype="multipart/form-data">

                <div class="profile-user" style="text-align: center; margin-bottom:8px;"><img id="mostraImagem" src="../dist/imagens/no-image-icon.png" width="130" height="130" style="  border-radius: 50%;"></div>
                <?php if (isset($_SESSION['mensagem'])) : ?>
                    <div class="warning">
                        <div class="alert alert-<?= $_SESSION['tipo_mensagem'] ?>">
                            <?php
                                echo $_SESSION['mensagem'];
                                unset($_SESSION['mensagem']);
                                ?>
                        </div>
                    </div>
                <?php endif ?>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Usuario" name="username" autocomplete="off">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="Email" name="email" autocomplete="off">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="senha" autocomplete="off">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Retype password" name="c_senha" autocomplete="off">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="file" class="form-control" name="imagemInput" id="imagemInput" accept="image/jpg,image/jpeg,image/png">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <input type="hidden" class="form-control" name="acao" value="2">
                <div class="row">

                    <!-- /.col -->

                    <div class="box-footer">

                        <a href="http://localhost/irrigatech_git/rafael-garcia/index.php" class="btn btn-default">Login</a>
                        <button type="submit" class="btn btn-primary pull-right">Registrar</button>
                    </div>

                    <!-- /.col -->
                </div>
            </form>


            <div class="social-auth-links text-center">

            </div>

            <!--<a href="login.html" class="text-center">I already have a membership</a>;-->
        </div>
        <!-- /.form-box -->
    </div>
    <!-- /.register-box -->

    <!-- jQuery 3 -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#mostraImagem').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imagemInput").change(function() {
            readURL(this);
        });
    </script>
</body>

</html>