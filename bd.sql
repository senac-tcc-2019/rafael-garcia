
CREATE SCHEMA IF NOT EXISTS irrigatech;

USE irrigatech ;


CREATE TABLE tb_usuario (
  idtb_usuario INT(11) NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) NULL DEFAULT NULL,
  senha VARCHAR(30) NULL DEFAULT NULL,
  username VARCHAR(20) NULL DEFAULT NULL,
  imagem LONGTEXT NOT NULL,
  token VARCHAR(100) NOT NULL,
  tokenExpire DATETIME NOT NULL,
  PRIMARY KEY (idtb_usuario));


CREATE TABLE tb_canteiro (
  idtb_canteiro INT(11) NOT NULL AUTO_INCREMENT,
  local VARCHAR(80) NULL DEFAULT NULL,
  umidade FLOAT NULL DEFAULT NULL,
  hora TIME NULL DEFAULT NULL,
  data DATE NULL DEFAULT NULL,
  tb_usuario_idtb_usuario INT(11) NOT NULL,
  PRIMARY KEY (idtb_canteiro),
  FOREIGN KEY (tb_usuario_idtb_usuario)REFERENCES tb_usuario (idtb_usuario));

CREATE TABLE tb_dispositivo (
  iddispositivo INT(11) NOT NULL AUTO_INCREMENT,
  DtCriacao DATE NULL DEFAULT NULL,
  status INT(11) NULL DEFAULT NULL,
  TempAtividade VARCHAR(45) NULL DEFAULT NULL,
  QtdAgua INT(11) NULL DEFAULT NULL,
  TempTotalirrigacao VARCHAR(45) NULL DEFAULT NULL,
  tb_canteiro_idtb_canteiro INT(11) NOT NULL,
  PRIMARY KEY (iddispositivo),
  FOREIGN KEY (tb_canteiro_idtb_canteiro)REFERENCES tb_canteiro (idtb_canteiro));


CREATE TABLE tb_agendamento (
  idtb_agendamento INT(11) NOT NULL AUTO_INCREMENT,
  dataInicio TIME NULL DEFAULT NULL,
  dataTermino TIME NULL DEFAULT NULL,
  tb_canteiro_idtb_canteiro INT(11) NOT NULL,
  tb_usuario_idtb_usuario INT(11) NOT NULL,
  cor VARCHAR (100),
  PRIMARY KEY (idtb_agendamento),
  FOREIGN KEY (tb_canteiro_idtb_canteiro)REFERENCES tb_canteiro (idtb_canteiro),
  FOREIGN KEY (tb_usuario_idtb_usuario)REFERENCES tb_usuario (idtb_usuario));

CREATE TABLE tb_planta (
  idplanta INT(11) NOT NULL AUTO_INCREMENT,
  tipo VARCHAR(100) NULL DEFAULT NULL,
  QtdAgua FLOAT NULL DEFAULT NULL,
  detalhe VARCHAR(150) NULL DEFAULT NULL,
  tb_canteiro_idtb_canteiro INT(11) NOT NULL,
  PRIMARY KEY (idplanta),
  FOREIGN KEY (tb_canteiro_idtb_canteiro)REFERENCES tb_canteiro (idtb_canteiro));




BUSCAS
SELECT 

SELECT tb_canteiro.local,
       tb_canteiro.umidade,
       tb_usuario.idtb_usuario from tb_canteiro inner join tb_usuario on
(tb_usuario.idtb_usuario = tb_canteiro.idtb_canteiro) WHERE tb_usuario.idtb_usuario = 2;

SELECT tb_canteiro.local,
       tb_dispositivo.status,
       tb_dispositivo.DtCriacao,
       tb_usuario.idtb_usuario,
       tb_dispositivo.iddispositivo from tb_canteiro inner join tb_usuario on
       (tb_usuario.idtb_usuario = tb_canteiro.idtb_canteiro) inner join tb_dispositivo on
       (tb_canteiro.idtb_canteiro = tb_dispositivo.iddispositivo) WHERE tb_usuario.idtb_usuario = 1;

SELECT tb_canteiro.idtb_canteiro,
        tb_canteiro.local
        from tb_canteiro inner join tb_usuario on
        (tb_canteiro.idtb_canteiro = tb_usuario.idtb_usuario ) WHERE tb_usuario.idtb_usuario = 2;


 SELECT 
 tb_canteiro.local,
 tb_agendamento.idtb_agendamento,
 tb_usuario.idtb_usuario,
 tb_usuario.username,
 tb_agendamento.cor,
 tb_agendamento.dataInicio,
 tb_agendamento.dataTermino
 from tb_agendamento inner join tb_usuario on 
 (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario) 
 inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro )where tb_agendamento.tb_usuario_idtb_usuario = 2;



SELECT
     tb_canteiro.local,
     tb_agendamento.idtb_agendamento,
     tb_agendamento.cor,
     tb_agendamento.dataInicio,
     tb_agendamento.dataTermino
     from tb_agendamento inner join tb_usuario on 
     (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario) 
     inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro )
      where tb_agendamento.tb_usuario_idtb_usuario ;


select tb_agendamento.idtb_agendamento,
       tb_dispositivo.iddispositivo,
       tb_usuario.idtb_usuario
       from tb_dispositivo inner join 
       tb_canteiro on (tb_dispositivo.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro)
       inner join tb_agendamento on (tb_agendamento.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro)
       inner join tb_usuario on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro)
        where  tb_dispositivo.iddispositivo = 3; 


select tb_canteiro.idtb_canteiro,
       tb_dispositivo.iddispositivo
       from tb_canteiro inner join tb_dispositivo on 
       (tb_dispositivo.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro);


select tb_agendamento.idtb_agendamento,
       tb_usuario.username,
       tb_canteiro.idtb_canteiro
       from tb_agendamento inner join tb_usuario on 
       (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario)
       inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro)
       where  tb_agendamento.tb_usuario_idtb_usuario = 1 and tb_canteiro.tb_usuario_idtb_usuario= 1;

SELECT tb_canteiro.local,
       tb_canteiro.idtb_canteiro,
       tb_dispositivo.iddispositivo,
       tb_usuario.idtb_usuario,
       tb_agendamento.dataTermino 
       from tb_canteiro inner join tb_usuario on
       (tb_usuario.idtb_usuario = tb_canteiro.idtb_canteiro)
       inner join tb_dispositivo on (tb_dispositivo.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro)
       inner join tb_agendamento on (tb_agendamento.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro)
        WHERE tb_usuario.idtb_usuario = 2 and tb_dispositivo.iddispositivo = 3;




SELECT tb_usuario.username,
       SUM(tb_dispositivo.iddispositivo) AS DISPOSITIVOS,
       SUM(tb_agendamento.idtb_agendamento) AS AGENDAMENTOS from tb_canteiro inner join tb_usuario on
       (tb_usuario.idtb_usuario = tb_canteiro.idtb_canteiro) inner join 
       tb_dispositivo on (tb_dispositivo.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro) 
       inner join tb_agendamento on(tb_agendamento.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro)
       ;
SELECT tb_usuario.username,
     COUNT(tb_dispositivo.iddispositivo) AS dispositivos,
     COUNT(tb_agendamento.idtb_agendamento) AS AGENDAMENTOS,
     SUM(tb_dispositivo.QtdAgua) AS QtdAgua,
     SUM(TempTotalirrigacao )
     from tb_canteiro inner join tb_usuario on
                (tb_usuario.idtb_usuario = tb_canteiro.tb_usuario_idtb_usuario) inner join 
                tb_dispositivo on (tb_dispositivo.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro) 
                inner join tb_agendamento on(tb_agendamento.tb_canteiro_idtb_canteiro = tb_canteiro.idtb_canteiro) where tb_canteiro.tb_usuario_idtb_usuario = 1 ;

SELECT tb_canteiro.local,
                                        tb_usuario_idtb_usuario,
                                        COUNT(tb_dispositivo.iddispositivo) as dispositivos,
                                        tb_dispositivo.status,
                                        tb_dispositivo.DtCriacao
                                        FROM tb_canteiro inner join tb_usuario on (tb_usuario.idtb_usuario = tb_canteiro.tb_usuario_idtb_usuario) inner join
                                        tb_dispositivo on (tb_canteiro_idtb_canteiro = tb_canteiro.tb_usuario_idtb_usuario )
                                        where tb_canteiro.tb_usuario_idtb_usuario = 1;

SELECT 
                                       COUNT(tb_agendamento.idtb_agendamento)
                                        from tb_agendamento inner join tb_usuario on 
                                        (tb_agendamento.tb_usuario_idtb_usuario = tb_usuario.idtb_usuario) 
                                        inner join tb_canteiro on (tb_agendamento.tb_usuario_idtb_usuario = tb_canteiro.idtb_canteiro )where tb_agendamento.tb_usuario_idtb_usuario = 1;                                      



SELECT AVG(valor) FROM tb_umidade where tb_canteiro_idtb_canteiro = 1 and data = "2019-11-06" ;                                        