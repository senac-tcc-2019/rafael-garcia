        <script>
            document.addEventListener('DOMContentLoaded', function () {
                var calendarEl = document.getElementById('calendar');

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    locale: 'pt-br',
                    plugins: ['interaction', 'dayGrid'],
                    //defaultDate: '2019-04-12',
                    editable: true,
                    eventLimit: true,
                    events: '../controller/agendamentoController.php',
                    extraParams: function () {
                        return {
                            cachebuster: new Date().valueOf()
                        };
                    },
                    eventClick: function(info) {
                         info.jsEvent.preventDefault();
                        $("#visualizar #id").text(info.event.id);
                        $("#visualizar").modal('show');

                     
                        }
                });

                calendar.render();
            });

        </script>