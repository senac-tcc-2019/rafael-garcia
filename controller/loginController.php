<?php
session_start();
include '../dao/conexao.php';


if ($_POST['acao'] == 1) {

    $email = $_POST['email'];
    $senha = $_POST['senha'];

    $sql = "SELECT idtb_usuario,email,senha,username FROM tb_usuario WHERE email= '$email'";
    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

    if (mysqli_num_rows($result) == 1) {
        while ($dado = mysqli_fetch_assoc($result)) {
            if (($dado['email'] == $email) && ($dado['senha'] == $senha)) {

                echo $_SESSION['username'] = $dado['username'];
                echo $_SESSION['senha'] = $dado['senha'];
                echo $_SESSION['idtb_usuario'] = $dado['idtb_usuario'];

                //Direciona para Home local
                 header("Location:http://localhost/irrigatech_git/rafael-garcia/view/homeView.php");
                //header("Location:http://sistemadeirrigacao.xyz/view/homeView.php");
            } else {

                unset($_SESSION['username']);
                unset($_SESSION['senha']);

                $_SESSION['mensagem'] = "Login ou senha incorretos.";
                $_SESSION['tipo_mensagem'] = "danger";
                //Redireciona para a pagina login local
                header("Location:http://localhost/irrigatech_git/rafael-garcia/index.php");

            }
        }
    } else {
        $_SESSION['mensagem'] = "Usuario Não encontrado!";
        $_SESSION['tipo_mensagem'] = "danger";
        //Redireciona para a pagina login local
         header("Location:http://localhost/irrigatech_git/rafael-garcia/index.php");
        
      //  header("Location:http://sistemadeirrigacao.xyz/index.php");
    }
}


if ($_POST['acao'] == 2) {

    $email = $_POST['email'];
    $username = $_POST['username'];
    $senha = $_POST['senha'];
    //Confirmação de senha
    $c_senha = $_POST['c_senha'];

    if ($senha == $c_senha) {

        $sql = "SELECT username,email FROM tb_usuario WHERE username = '" . $username . "' and email = '" . $email . "'";
        $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
        //Veficia se usuario ja existe
        if ($result < 0) {
            $_SESSION['mensagem'] = "Usuario ja existe!";
            $_SESSION['tipo_mensagem'] = "danger";
        } else {
            //Verifica se imagem foi selecionada
            if ($_FILES['imagemInput']) {

                $arquivoInput = explode(".", $_FILES['imagemInput']['name']);
                $novoNomeArquivo = $username . round(microtime(true)) . "." . end($arquivoInput);

                $arquivo = $_FILES['imagemInput'];

                $dirUploads = "../dist/imagens";
                if (!is_dir($dirUploads)) {
                    mkdir($dirUploads);
                }
                //Se imagem estiver selecionada, insere informação no banco
                if (move_uploaded_file($arquivo["tmp_name"], $dirUploads . DIRECTORY_SEPARATOR . $novoNomeArquivo)) {

                    $sql = "INSERT INTO tb_usuario(username,email,senha,imagem)VALUES('$username','$email','$senha','$novoNomeArquivo')";
                    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                    if ($result) {
                        //Busca usuario recem cadastrado e o redireciona para a home local
                        $sql = "SELECT idtb_usuario,senha,email,username FROM tb_usuario WHERE username= '$username'";
                        $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                        while ($dado = mysqli_fetch_assoc($result)) {

                            if (($dado['senha'] == $senha) && ($dado['username'] == $username)) {

                                echo $_SESSION['username'] = $dado['username'];
                                echo $_SESSION['senha'] = $dado['senha'];
                                echo $_SESSION['idtb_usuario'] = $dado['idtb_usuario'];
                                //Redireciona para a home local
                                header("Location:http://localhost/irrigatech_git/rafael-garcia/view/homeView.php");
                                //header("Location:http://sistemadeirrigacao.xyz/view/homeView.php");
                            }
                        }
                    } else {
                        $_SESSION['mensagem'] = "Ocorreu um erro ao cadastrar!";
                        $_SESSION['tipo_mensagem'] = "danger";

                        //Redireciona para a pagina login local
                         header("Location:http://localhost/irrigatech_git/rafael-garcia/index.php");
                        //header("Location:http://sistemadeirrigacao.xyz/index.php");
                    }
                }
            }
        }
    } else {
        echo "Verifique as senhas";
    }
}
