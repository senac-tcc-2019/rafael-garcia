<?php

session_start();
unset($_SESSION['username']);
unset($_SESSION['senha']);

session_destroy();

header("location: http://localhost/irrigatech_git/rafael-garcia/index.php");
exit;
